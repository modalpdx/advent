#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>


#define ARRAY_AXIS 500

int main(int argc, char **argv)
{
    // Set up a huge array matrix to cover all houses.
    static int gifts[ARRAY_AXIS][ARRAY_AXIS];

    // File pointer, character to read.
    FILE *fp;
    char c;

    // Number of houses with more than one present.
    int results = 0;

    // Start in the middle of the array matrix.
    int arrX, arrY; 

    // Open the file.
    fp = fopen(argv[1], "r");
    if(!fp)
        exit(1);

    // Initialize the array matrix with 0s.
    for(arrY = 0; arrY < ARRAY_AXIS; arrY++)
    {
        for(arrX = 0; arrX < ARRAY_AXIS; arrX++)
        {
            gifts[arrY][arrX] = 0;
        }
    }

    // Start at the middle of the array matrix.
    arrX = arrY = round(ARRAY_AXIS/2);

    // You give a gift where you start
    gifts[arrY][arrX] = 1;

    // Read characters from the input, act accordingly.
    while((c = fgetc(fp)) != EOF)
    {
        switch(c)
        {
            case '^':
                arrY++;
                break;
            case '>':
                arrX++;
                break;
            case 'v':
                arrY--;
                break;
            case '<':
                arrX--;
        }
        gifts[arrY][arrX] += 1;
    }

    // Close the file.
    fclose(fp);

    // Tally the results.
    for(arrY = 0; arrY < ARRAY_AXIS; arrY++)
    {
        for(arrX = 0; arrX < ARRAY_AXIS; arrX++)
        {
            if(gifts[arrY][arrX] > 1)
            {
                results++;
            }
        }
    }

    printf("Houses with more than one present: %d\n", results);

    return 0;
}
