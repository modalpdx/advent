#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char **argv)
{
    FILE *fp;
    char input[20];
    //const char *pairs[] = {"ab", "cd", "pq" "xy"};
    int inputLen    = 0;
    int idx         = 0;
    int totalNice   = 0;
    int numVowels   = 0;
    int numDbls     = 0;
    int numBadPairs = 0;

    if(argc < 2)
    {
        fprintf(stderr, "Usage: %s [input file]\n", argv[0]);
        exit(1);

    }

    if((fp = fopen(argv[1], "r")) == NULL)
    {
        fprintf(stderr, "Error opening input file.\n");
        exit(1);
    }

    while(fgets(input, sizeof(input), fp))
    {
        // Get length of input string so we don't have
        // to do this over and over again.
        //
        inputLen = strlen(input);

        // Get rid of the newline. Not essential.
        //
        input[inputLen - 1] = '\0';

        // Count vowels.
        //
        for(idx = 0; idx < inputLen; idx++)
        {
            switch(input[idx])
            {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                    numVowels++;
            }
        }

        // Count letter doubles.
        //
        for(idx = 1; idx < inputLen; idx++)
        {
            if(input[idx] == input[idx-1])
                numDbls++;
        }

        // Count illegal pairs (UGLY!! There has to be a better way.
        // array of string pointers didn't work!)
        //
        if((strstr(input, "ab") != NULL) ||
           (strstr(input, "cd") != NULL) ||
           (strstr(input, "pq") != NULL) ||
           (strstr(input, "xy") != NULL))
            numBadPairs++;

        // Is input "nice"?
        //
        if((numVowels >= 3) && (numDbls >= 1) && (numBadPairs == 0))
            totalNice++;

        // Reset the input string, just because...
        //
        input[0] = '\0';

        // Reset everything
        //
        numVowels = numDbls = numBadPairs = inputLen = 0;
    }

    // Close the file.
    //
    fclose(fp);

    // Print the results.
    //
    printf("Total nice: %d\n", totalNice);

    return 0;
}
