#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>

// If your secret key is abcdef, the answer is 609043 because
// the MD5 has of "abcdef609043" starts with at least five
// zeroes (000001dbbfa...).

int main(int argc, char **argv)
{

    // Prefix will be "ckczppom" in the final contest.

    char *delim = "00000";
    char input[100];
    char inputNum[100];
    char mdString[33];
    unsigned char digest[MD5_DIGEST_LENGTH];
    unsigned long idx;
    int idx2;

    if(argc < 2)
    {
        printf("Usage: %s [prefix]\n", argv[0]);
        exit(1);
    }

    idx = 0;
    do
    {
        // Make sure we're starting with new strings
        //
        input[0] = '\0';
        inputNum[0] = '\0';
        
        // Add arg1 to input string
        //
        strcat(input, argv[1]);

        // Add the index number to the input string
        //
        sprintf(inputNum, "%ld", idx++);
        strcat(input, inputNum);

        // Hash it...
        //
        MD5((unsigned char *)input, strlen(input), (unsigned char *)&digest);

        // Convert to hex
        //
        for(idx2 = 0; idx2 < 16; idx2++)
             sprintf(&mdString[idx2*2], "%02x", (unsigned int)digest[idx2]);

    } while(strncmp(mdString, delim, 5) != 0);

    if(strncmp(mdString, delim, 5) == 0)
    {
        // -1...sloppy! Maybe fix this some day. Initialize
        // idx differently, or iterate it differently, or...
        //
        printf("%ld works: %s\n", idx-1, mdString);
    }

    return 0;
}




