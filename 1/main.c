/*
--- Day 1: Not Quite Lisp ---

Here's an easy puzzle to warm you up.

Santa is trying to deliver presents in a large apartment building, but he can't find the right floor - the directions he got are a little confusing. He starts on the ground floor (floor 0) and then follows the instructions one character at a time.

An opening parenthesis, (, means he should go up one floor, and a closing parenthesis, ), means he should go down one floor.

The apartment building is very tall, and the basement is very deep; he will never find the top or bottom floors.

For example:

(()) and ()() both result in floor 0.
((( and (()(()( both result in floor 3.
))((((( also results in floor 3.
()) and ))( both result in floor -1 (the first basement level).
))) and )())()) both result in floor -3.
To what floor do the instructions take Santa?
*/

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv)
{
    FILE *fp;
    char c;
    int floor = 0;

    fp = fopen(argv[1], "r");

    if(!fp)
    {
        exit(1);
    }

    while((c = fgetc(fp)) != EOF)
    {
        if(c == '(') 
            floor++;
        else
            floor--;
    }

    printf("Floor: %d\n", floor);
    
    fclose(fp);

    return 0;
    
}
