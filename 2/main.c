#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
    FILE *fp;
    char line[10];
    char *token;
    char delim[2] = "x";
    int l, w, h, side, slack, total_gift, total_total;

    l = w = h = side = slack = total_gift = total_total = 0;

    fp = fopen(argv[1], "r");

    if(fp == NULL)
        exit(1);

    // Calculate how much wrapping paper is needed.
    // Read a line of dimensions, then calculate
    // the area with the following math:
    //
    // 2*l*w + 2*w*h + 2*h*l
    //
    // Each line in the input is LxWxH.
    //
    //
    while(fgets(line, sizeof(line), fp) != NULL)
    {
        line[strlen(line)-1] = '\0';

        // Get the length
        token = strtok(line, delim);
        l = atoi(token);

        // Get the width
        token = strtok(NULL, delim);
        w = atoi(token);

        // Get the height
        token = strtok(NULL, delim);
        h = atoi(token);

        // Get first measurement, start off slack
        side = l * w;
        slack = side;
        total_gift += 2 * side;

        // Get second measurement
        side = w * h;
        if(side < slack)
            slack = side;
        total_gift += 2 * side;

        // Get third measurement
        side = h * l;
        if(side < slack)
            slack = side;
        total_gift += 2 * side;

        // Add slack
        total_gift += slack;

        // Add gift total to TOTAL total
        total_total += total_gift;

        // Reset gift total
        total_gift = 0;
    }

    printf("Total: %d\n", total_total);

    fclose(fp);

    return 0;

}

